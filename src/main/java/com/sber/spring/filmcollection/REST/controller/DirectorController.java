package com.sber.spring.filmcollection.REST.controller;

import com.sber.spring.filmcollection.dto.AddFilmDTO;
import com.sber.spring.filmcollection.dto.DirectorDTO;
import com.sber.spring.filmcollection.model.Director;
import com.sber.spring.filmcollection.service.DirectorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/directors")
@Tag(name = "Режиссеры",
        description = "Контроллер для работы с режиссерами фильмов фильмотеки")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DirectorController
        extends GenericController<Director, DirectorDTO> {

    private final DirectorService directorService;

    public DirectorController(DirectorService directorService) {
        super(directorService);
        this.directorService = directorService;
    }

    @Operation(description = "Добавить фильм к режиссеру", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDTO> addDirector(@RequestBody AddFilmDTO addFilmDTO) {
        directorService.addFilm(addFilmDTO);
        return ResponseEntity.status(HttpStatus.OK).body(directorService.getOne(addFilmDTO.getDirectorId()));
    }
}



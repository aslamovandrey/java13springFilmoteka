package com.sber.spring.filmcollection.dto;

import com.sber.spring.filmcollection.model.Genre;
import groovy.transform.ToString;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ToString
public class FilmSearchDTO {
    private String filmTitle;
    private String directorsFio;
    private Genre genre;
}


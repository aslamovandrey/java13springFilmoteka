package com.sber.spring.filmcollection.dto;

import com.sber.spring.filmcollection.model.Genre;
import lombok.*;

import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FilmDTO extends GenericDTO {
    private String filmTitle;
    private String premierYear;
    private String country;
    private Genre genre;
    private Set<Long> directorsIds;
    private boolean isDeleted;

}

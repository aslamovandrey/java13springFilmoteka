package com.sber.spring.filmcollection.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public abstract class GenericDTO {
    private Long id;
}

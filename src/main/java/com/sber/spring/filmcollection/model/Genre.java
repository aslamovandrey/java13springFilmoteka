package com.sber.spring.filmcollection.model;
public enum Genre {
    FANTASY("Фантастика"),
    DRAMA("Драма"),
    COMEDY("Комедия"),
    THRILLER("Триллер");

    private final String genreTextDisplay;

    Genre(String genreName) {
        this.genreTextDisplay = genreName;
    }

    public String getGenreTextDisplay() {
        return this.genreTextDisplay;
    }
}

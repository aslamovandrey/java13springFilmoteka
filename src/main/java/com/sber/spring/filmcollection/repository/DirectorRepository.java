package com.sber.spring.filmcollection.repository;

import com.sber.spring.filmcollection.model.Director;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {
    Page<Director> findAllByDirectorsFioContainsIgnoreCase(String fio, Pageable pageable);
}

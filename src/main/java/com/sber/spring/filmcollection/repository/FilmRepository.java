package com.sber.spring.filmcollection.repository;

import com.sber.spring.filmcollection.model.Film;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface FilmRepository extends GenericRepository<Film> {
    @Query(nativeQuery = true,
            value = """
                 select distinct b.*
                 from films b
                 left join films_directors ba on b.id = ba.film_id
                 join directors a on a.id = ba.director_id
                 where b.title ilike '%' || btrim(coalesce(:title, b.title)) || '%'
                 and cast(b.genre as char) like coalesce(:genre,'%')
                 and a.directors_fio ilike '%' || :directors_fio || '%'
                      """)

    Page<Film> searchFilms(@Param(value = "genre") String genre,
                           @Param(value = "title") String title,
                           @Param(value = "directors_fio") String directors_fio,
                           Pageable pageable);

}

package com.sber.spring.filmcollection.repository;

import com.sber.spring.filmcollection.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends GenericRepository<Order> {
    Page<Order> getOrderByUserId(Long userId,
                                        Pageable pageable);

}

package com.sber.spring.filmcollection.repository;

import com.sber.spring.filmcollection.model.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends GenericRepository<Role> {
}

package com.sber.spring.filmcollection;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class FilmcollectionApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(FilmcollectionApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }
}

package com.sber.spring.filmcollection.exception;

public class MyDeleteException extends Exception{
    public MyDeleteException(String message) {
        super(message);
    }
}

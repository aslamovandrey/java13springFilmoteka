package com.sber.spring.filmcollection.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String LIBRARIAN = "LIBRARIAN";
    String USER = "USER";
}

package com.sber.spring.filmcollection.service;

import com.sber.spring.filmcollection.constants.MailConstants;
import com.sber.spring.filmcollection.dto.FilmDTO;
import com.sber.spring.filmcollection.dto.RoleDTO;
import com.sber.spring.filmcollection.dto.UserDTO;
import com.sber.spring.filmcollection.exception.MyDeleteException;
import com.sber.spring.filmcollection.mapper.FilmMapper;
import com.sber.spring.filmcollection.mapper.UserMapper;
import com.sber.spring.filmcollection.model.Film;
import com.sber.spring.filmcollection.model.Order;
import com.sber.spring.filmcollection.model.User;
import com.sber.spring.filmcollection.repository.FilmRepository;
import com.sber.spring.filmcollection.repository.UserRepository;
import com.sber.spring.filmcollection.utils.MailUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static com.sber.spring.filmcollection.constants.UserRolesConstants.ADMIN;

@Service
public class UserService
        extends GenericService<User, UserDTO>{
    private final JavaMailSender javaMailSender;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final FilmMapper filmMapper;
    private final FilmRepository filmRepository;

    protected UserService(UserRepository userRepository, UserMapper userMapper,
                          FilmRepository filmRepository, FilmMapper filmMapper,
                          BCryptPasswordEncoder bCryptPasswordEncoder,
                          JavaMailSender javaMailSender) {
        super(userRepository, userMapper);
        this.filmMapper = filmMapper;
        this.filmRepository = filmRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.javaMailSender = javaMailSender;
    }

    public UserDTO getUserByLogin(final String login) {
        return mapper.toDTO(((UserRepository) repository).findUserByLogin(login));
    }

    public UserDTO getUserByEmail(final String email) {
        return mapper.toDTO(((UserRepository) repository).findUserByEmail(email));
    }

    public Boolean checkPassword(String password, UserDetails userDetails) {
        return bCryptPasswordEncoder.matches(password, userDetails.getPassword());
    }

    @Override
    public UserDTO create(UserDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if (ADMIN.equalsIgnoreCase(userName)) {
            roleDTO.setId(2L);//библиотекарь
        }
        else {
            roleDTO.setId(1L);//пользователь
        }
        object.setRole(roleDTO);
        object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public List<FilmDTO> getAllFilms(Long userId) {
        User user = repository.findById(userId).orElseThrow(() -> new NotFoundException("Пользователь не найден"));
        Set<Order> orders = user.getOrders();
        List<Film> films = orders.stream().map(Order::getFilm).toList();
        return filmMapper.toDTOs(films);
    }

    public void sendChangePasswordEmail(final UserDTO userDTO) {
        UUID uuid = UUID.randomUUID();
        userDTO.setChangePasswordToken(uuid.toString());
        update(userDTO);
        SimpleMailMessage mailMessage = MailUtils.createEmailMessage(userDTO.getEmail(),
                MailConstants.MAIL_SUBJECT_FOR_REMEMBER_PASSWORD,
                MailConstants.MAIL_MESSAGE_FOR_REMEMBER_PASSWORD + uuid);
        javaMailSender.send(mailMessage);
    }

    public void changePassword(final String uuid,
                               final String password) {
        UserDTO user = mapper.toDTO(((UserRepository) repository).findUserByChangePasswordToken(uuid));
        user.setChangePasswordToken(null);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        update(user);
    }

    public Page<UserDTO> findUsers(UserDTO userDTO,
                                   Pageable pageable) {
        Page<User> users = ((UserRepository) repository).searchUsers(userDTO.getFirstName(),
                userDTO.getLastName(),
                userDTO.getLogin(),
                pageable);
        List<UserDTO> result = mapper.toDTOs(users.getContent());
        return new PageImpl<>(result, pageable, users.getTotalElements());
    }

    public void delete(Long id) throws MyDeleteException {
        repository.deleteById(id);
    }
}

package com.sber.spring.filmcollection.service;

import com.sber.spring.filmcollection.dto.GenericDTO;
import com.sber.spring.filmcollection.exception.MyDeleteException;
import com.sber.spring.filmcollection.mapper.GenericMapper;
import com.sber.spring.filmcollection.model.GenericModel;
import com.sber.spring.filmcollection.repository.GenericRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {

    protected final GenericRepository<T> repository;
    protected final GenericMapper<T, N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<T> repository,
                             GenericMapper<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<N> listAll() {
        return mapper.toDTOs(repository.findAll());
    }

    public Page<N> listAll(Pageable pageable) {
        Page<T> objects = repository.findAll(pageable);
        List<N> result = mapper.toDTOs(objects.getContent());
        return new PageImpl<>(result, pageable, objects.getTotalElements());
    }

    public N getOne(final Long id) {
        return mapper.toDTO(repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Данных по заданному id: " + id + " не найдены")));
    }

    public N create(N object) {
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public N update(N object) {
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public void delete(Long id) throws MyDeleteException {
        repository.deleteById(id);
    }
}



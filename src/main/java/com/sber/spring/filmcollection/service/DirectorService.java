package com.sber.spring.filmcollection.service;

import com.sber.spring.filmcollection.dto.AddFilmDTO;
import com.sber.spring.filmcollection.dto.DirectorDTO;
import com.sber.spring.filmcollection.mapper.DirectorMapper;
import com.sber.spring.filmcollection.model.Director;
import com.sber.spring.filmcollection.repository.DirectorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class DirectorService
        extends GenericService<Director, DirectorDTO>{
    private final DirectorRepository directorRepository;
    private final FilmService filmService;

    protected DirectorService(DirectorRepository directorRepository,
                              DirectorMapper directorMapper,
                              FilmService filmService) {
        super(directorRepository, directorMapper);
        this.directorRepository = directorRepository;
        this.filmService = filmService;
    }

    public Page<DirectorDTO> searchDirectors(final String fio,
                                           Pageable pageable) {
        Page<Director> directors = directorRepository.findAllByDirectorsFioContainsIgnoreCase(fio, pageable);
        List<DirectorDTO> result = mapper.toDTOs(directors.getContent());
        return new PageImpl<>(result, pageable, directors.getTotalElements());
    }
    public void addFilm(AddFilmDTO addFilmDTO) {
        DirectorDTO director = getOne(addFilmDTO.getDirectorId());
        filmService.getOne(addFilmDTO.getFilmId());
        director.getFilmsIds().add(addFilmDTO.getFilmId());
        update(director);
    }
}

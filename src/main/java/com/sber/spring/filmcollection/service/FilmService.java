package com.sber.spring.filmcollection.service;

import com.sber.spring.filmcollection.constants.Errors;
import com.sber.spring.filmcollection.dto.FilmDTO;
import com.sber.spring.filmcollection.dto.FilmSearchDTO;
import com.sber.spring.filmcollection.dto.FilmWithDirectorsDTO;
import com.sber.spring.filmcollection.exception.MyDeleteException;
import com.sber.spring.filmcollection.mapper.FilmMapper;
import com.sber.spring.filmcollection.mapper.FilmWithDirectorsMapper;
import com.sber.spring.filmcollection.model.Director;
import com.sber.spring.filmcollection.model.Film;
import com.sber.spring.filmcollection.repository.DirectorRepository;
import com.sber.spring.filmcollection.repository.FilmRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
@Slf4j
public class FilmService
        extends GenericService<Film, FilmDTO> {
    private final FilmRepository filmRepository;
    private final DirectorRepository directorRepository;
    private final FilmWithDirectorsMapper filmWithDirectorsMapper;

    protected FilmService(FilmRepository filmRepository, DirectorRepository directorRepository,
                          FilmMapper filmMapper, FilmWithDirectorsMapper filmWithDirectorsMapper) {
        super(filmRepository, filmMapper);
        this.filmRepository = filmRepository;
        this.directorRepository = directorRepository;
        this.filmWithDirectorsMapper = filmWithDirectorsMapper;
    }

    public FilmDTO addDirector(Long filmId, Long directorId) {
        Film film = repository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм не найден"));
        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Режиссёр не найден"));
        film.getDirectors().add(director);
        return mapper.toDTO(repository.save(film));
    }

    public Page<FilmWithDirectorsDTO> getAllFilmsWithDirectors(Pageable pageable) {
        Page<Film> filmsPaginated = repository.findAll(pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }

    public FilmWithDirectorsDTO getFilmWithDirectors(Long id) {
        return filmWithDirectorsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }

    public FilmDTO update(final FilmDTO object) {
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    @Override
    public void delete(Long id) throws MyDeleteException {
        Film film = repository.findById(id).orElseThrow(
                () -> new NotFoundException("Фильм с заданным ID=" + id + " не существует"));
        if (film.getOrders().size() > 0) {
            throw new MyDeleteException(Errors.Films.FILM_DELETE_ERROR);
        } else {
            repository.deleteById(film.getId());
        }
    }

    public Page<FilmWithDirectorsDTO> findFilms(FilmSearchDTO filmSearchDTO,
                                                Pageable pageable) {
        String genre = filmSearchDTO.getGenre() != null ? String.valueOf(filmSearchDTO.getGenre().ordinal()) : null;
        Page<Film> filmsPaginated = filmRepository.searchFilms(genre,
                filmSearchDTO.getFilmTitle(),
                filmSearchDTO.getDirectorsFio(),
                pageable
        );
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }
}

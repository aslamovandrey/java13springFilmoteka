package com.sber.spring.filmcollection.mapper;

import com.sber.spring.filmcollection.dto.UserDTO;
import com.sber.spring.filmcollection.model.GenericModel;
import com.sber.spring.filmcollection.model.User;
import com.sber.spring.filmcollection.repository.OrderRepository;
import com.sber.spring.filmcollection.utils.DateFormatter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<User, UserDTO> {
    private OrderRepository orderRepository;

    protected UserMapper(ModelMapper mapper, OrderRepository orderRepository) {
        super(mapper, User.class, UserDTO.class);
        this.orderRepository = orderRepository;
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setOrdersIds)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(m -> m.skip(User::setOrders)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(User::setBirthDate)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
        if (!Objects.isNull(source.getOrdersIds()) && source.getOrdersIds().size() > 0) {
            destination.setOrders(new HashSet<>(orderRepository.findAllById(source.getOrdersIds())));
        } else {
            destination.setOrders(Collections.emptySet());
        }
        destination.setBirthDate(DateFormatter.formatStringToDate(source.getBirthDate()));
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        destination.setOrdersIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(User user) {
        return Objects.isNull(user) || Objects.isNull(user.getOrders())
                ? null
                : user.getOrders().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}

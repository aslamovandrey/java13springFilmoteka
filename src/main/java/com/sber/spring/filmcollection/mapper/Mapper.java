package com.sber.spring.filmcollection.mapper;

import com.sber.spring.filmcollection.dto.GenericDTO;
import com.sber.spring.filmcollection.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);

    List<E> toEntities(List<D> dtos);

    D toDTO(E entity);

    List<D> toDTOs(List<E> entities);
}


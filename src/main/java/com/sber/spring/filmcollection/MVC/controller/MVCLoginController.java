package com.sber.spring.filmcollection.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Hidden
@RequestMapping("/login")
public class MVCLoginController {
    @GetMapping("")
    public String login() {
        if (
                SecurityContextHolder.getContext().getAuthentication() != null &
                        SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
                        !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)
        ) {
            return "redirect:registration";
        }
        return "login";
    }
}

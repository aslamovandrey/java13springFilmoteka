package com.sber.spring.filmcollection.MVC.controller;

import com.sber.spring.filmcollection.dto.FilmDTO;
import com.sber.spring.filmcollection.model.Genre;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
@Rollback(value = false)
public class MVCFilmControllerTest extends CommonTestMVCTest {
    private final FilmDTO filmDTO = new FilmDTO("TestFilmTitle","2023-01-01", "TestCountry", Genre.COMEDY, new HashSet<>(), false);

    @Test
    @DisplayName("Просмотр, тестирование 'films/getAll()'")
    @Order(0)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void listAll() throws Exception {
        log.info("Тест начат успешно");
        mvc.perform(get("/films")
                        .param("page", "1")
                        .param("size", "5")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("films/viewAllFilms"))
                .andExpect(model().attributeExists("films"));
        log.info("Тест закончен успешно");
    }
    @Test
    @DisplayName("Создание, тестирование 'films/add'")
    @Order(1)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void createObject() throws Exception {
        log.info("Тест по созданию начат успешно");
        mvc.perform(post("/films/add")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("filmForm", filmDTO)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/films"))
                .andExpect(redirectedUrlTemplate("/films"))
                .andExpect(redirectedUrl("/films"));
        log.info("Тест по созданию закончен успешно");
    }

    @Order(2)
    @Test
    @DisplayName("Обновление, тестирование 'films/update'")
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void updateObject() throws Exception {
        log.info("Тест по обновлению начат успешно");
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "filmTitle"));
        mvc.perform(post("/films/update")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("filmForm", filmDTO)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/films"))
                .andExpect(redirectedUrl("/films"));
    }

    @Test
    @Order(3)
    @Override
    @DisplayName("Удаление, тестирование 'films/delete'")
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    protected void deleteObject() throws Exception {
        log.info("Тест по удалению начат успешно");
        mvc.perform(MockMvcRequestBuilders.get("/films/delete/{id}", "1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/films"));
        log.info("Тест по удалению закончен успешно");
    }
}

